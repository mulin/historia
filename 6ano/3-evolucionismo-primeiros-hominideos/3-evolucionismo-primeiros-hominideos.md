# Plano de Aula de História
# Evolucionismo e Primeiros Hominídeos
## 6º ano

**Prof. Mulin**

![](../../ccbysa4.jpg)

**Atribuição-CompartilhaIgual 4.0 Internacional (CC BY-SA 4.0)**

# Reflexão

- Quando começou o mundo?

- Quando surgiu o primeiro Homem?

> Estabelecer uma reflexão entre os alunos que existem outras crenças muito diferentes das cristãs e que têm outras visões de criação do mundo e das espécies. Apresentar outras visões criacionistas de outras religiões.

> A partir daí, estabelecer a separação entre o ensino científico que deve ser realizado na escola e as visões religiosas que devem ser respeitadas, mas não utilizadas como argumento nesse momento.

> Estabelecer uma ligação com a aula anterior sobre fontes históricas, nas quais o ensino científico se baseia e o separar do dogmatismo. Concentrar-se no conceito de **PARADIGMA CIENTÍFICO** e introduzir o evolucionismo.

## Evolucionismo

Evolucionismo é uma teoria elaborada e desenvolvida por diversos cientistas para explicar as alterações sofridas pelas diversas espécies de seres vivos ao longo do tempo, em sua relação com o meio ambiente onde elas habitam. O principal cientista ligado ao evolucionismo foi o inglês Charles Robert Darwin (1809-1882), que publicou, em 1859, a obra Sobre a origem das espécies por meio da seleção natural ou a conservação das raças favorecidas na luta pela vida, ou como é mais comumente conhecida, A Origem das Espécies.

Darwin pôde perceber que entre espécies extintas e espécies presentes no meio ambiente havia caracterı́sticas comuns. Isso o levou a afirmar que havia um caráter mutável entre as espécies, e não uma caracterı́stica imutável como antes era comum entender. As espécies não existem da mesma forma ao longo do tempo, elas evoluem. Durante a evolução, elas transmitem geneticamente essas mudanças às gerações posteriores.

Entretanto, para Darwin, evoluir é mudar biologicamente (e não necessariamente se tornar melhor), e as mudanças geralmente ocorrem para que exista uma adaptação das espécies ao meio ambiente em que vivem. A esse processo de mudança em consonância com o meio ambiente Charles Darwin deu o nome de **seleção natural**.

> A partir do conceito de evolucionismo, quebrar a crença dos alunos onde acreditam que a ciência afirma que "o homem veio do macaco". **A ciência nunca disse que o homem veio do macaco. O paradigma ciêntífico propõe que o homem originou-se de um ancestral comum aos macacos, mas não deles. Somos apenas da mesma ordem: a dos primatas**.

Nós não evoluı́mos dos macacos modernos. Nós compartilhamos um ancestral comum com os macacos. Da mesma forma que você não descende do seu primo, mas compartilha com ele um ancestral em comum.

> Apresentar imagens de algumas **fontes arquológicas** (conceito apresentado na aula anterior) que embasam os estudos sobre a Pré-História, que será assunto da próxima aula.