# Plano de Aula de História
# Pré-História: periodização e características dos grupos humanos
## 6º ano

**Prof. Mulin**

![](../../ccbysa4.jpg)

**Atribuição-CompartilhaIgual 4.0 Internacional (CC BY-SA 4.0)**

# Conceito

- A Pré-História é o período que vai do surgimento do Ser Humano até a invenção da escrita.
- Para estudar a Pré-História, recorre-se à **ARQUEOLOGIA** - ciência que estuda os vestígios deixados pelos seres humanos durante sua passagem pela Terra.

# Período Paleolítico ou Período da Pedra Lascada

- Período que vai de 1 milhão a.C. com o surgimento do homem e vai até 10.000 a.C.
- É assim denominado porque o homem nesse período utilizava utensílios de pedra lascada em seu cotidiano.
- É o mais longo período pré-histórico.
- Moravam em cavernas, tendas feitas de peles e ossos de animais, além de galhos e folhas.
- Eram **caçadores e coletores**
- Os homens do Paleolítico caçavam grandes animais como mamutes e cangurus gigantes, além de outros menores como gazelas.
- Também praticavam a **COLETA**, que consistia em coletar alimentos fornecidos pela própria natureza, como frutos e vegetais.
- O homem do paleolítico era **NÔMADE**, pois precisava migrar de lugares quando a caça e os alimentos da natureza ficavam escassos. Portanto, **não tinha moradia fixa**.
-   Inicialmente andavam nus, mas depois passaram a vestir-se com as peles de animais que caçavam.
- As pinturas rupestres e esculturas de pedra eram suas principais formas de manifestação artística.

## Divisão do trabalho

- O trabalho de caçar, pescar e construir era realizado pelos homens.
- O trabalho de coletar alimentos e cuidar das crianças cabia às mulheres, velhos e crianças maiores.
- Podemos dizer que isso representou a primeira divisão do trabalho.

## O domínio do fogo

- O domínio do fogo representa uma conquista importante do Paleolítico.
- Segundo historiadores, teria ocorrido há mais ou menos 500 mil anos nos vales vulcânicos da África Oriental.
- O homem do paleolítico para conseguir obter o fogo, buscavam incêndios naturais provocados por raios ou vulcões, pois ainda não sabiam produzir o fogo.
- Usavam o fogo para afastar animais, se aquecer durante o frio, iluminar o ambiente, assar alimentos, etc.

# Período Neolítico ou Período da Pedra Polida

- O termo neolítico, escolhido para denominar o período que vai de 10.000 a.C a 5.000 a.C, significa ``pedra nova''.
- O termo refere-se ao fato de os homens nessa época polirem as pedras na fabricação de armas e ferramentas.
- O homem do paleolítico para conseguir obter o fogo, buscavam incêndios naturais provocados por raios ou vulcões, pois ainda não sabiam produzir o fogo.

## Revolução Neolítica

- O desenvoluvimento da **AGRICULTURA (Revolução Agrícola)** e da **DOMESTICAÇÃO DE ANIMAIS E PASTOREIO** foram importantes conquistas do Neolítico.
- O homem passou a produzir seu próprio alimento através dessa técnicas.
- Dessa forma, não era mais necessário buscar alimentos em outros locais, permitindo que o homem deixasse aos poucos de ser nômade e passasse a ser **SEDENTÁRIO**. Ou seja, passaram a **ter moradias fixas**.
- A melhoria na alimentação proporcionada pela Revolução Neolítica favoreceu o aumento de crianças e idosos, que eram as maiores vítimas de abandonos das sociedades nômades.
- Com a prática da agricultura, os seres humanos passaram a necessitar de recipientes para armazenar cereais e alimentos. A invenção da cerâmica veio para atender essa necessidade.
- As antigas cavernas e cabanas feitas de peles e ossos de animais, além de folhas e palha, foram substituídas por casas de barro e madeira ou pedra.

## Religiosidade

- Após o sedentarismo e a prática da atividade agrícola, a terra tornou-se importante e ganhou muito significado para a vida dos neolíticos. Por isso, os rituais eram feitos como uma forma de pedido aos deuses ou de invocação de forças sobrenaturais para que a colheita tivesse êxito.
- Encontram-se vestígios na Europa Ocidental de uma civilização que nos deixou grandes monumentos de pedra (ou megalitos) dispersos através do continente - a cultura megalítica europeia. Calcula-se que as primeiras destas construções, encontradas na Península Ibérica, datem de aproximadamente 5000 a.C.,ou seja   cerca de dois milénios antes da construção das Pirâmides do Egito.
- Também existe a possibilidade de ter havido a primeira manifestação humana de uma religião a qual baseou-se no culto à mulher, ao feminino e a associação desta à Natureza, ao poder de dar a vida.

## Primeiras cidades

Cada vez mais havia necessidade de garantir o domínio sobre as terras férteis. O aperfeiçoamento de fabricação de utensílios do cotidiano e armas de metais possibilitou que algumas comunidades dominassem grandes extensões de terras produtivas. A população continuou aumentando e começaram a se formar as **primeiras cidades**.

## Aparecimento da escrita

- Uma escrita sistematizada aparece somente por volta de 3500 a.C., quando os sumérios desenvolveram a escrita cuneiforme na Mesopotâmia. Os registros cotidianos, econômicos e políticos da época eram feitos na argila, com símbolos formados por cones. Nesse mesmo momento, surgem os hieróglifos no Egito. Essa escrita era dominada apenas por pessoas poderosas da sociedade, como escribas e sacerdotes.
- As primeiras civilizações usaram uma série de suportes para a escrita que são impensáveis no mundo de hoje: barro, cascas de árvore, rochas, couro de animais, ossos…
- **O aparecimento da escrita encerra o período da Pré-História.**
