# Plano de Aula de História
# A Evolução do Ser Humano e Chegada do Homem à América
## 6º ano

**Prof. Mulin**

![](../../ccbysa4.jpg)

**Atribuição-CompartilhaIgual 4.0 Internacional (CC BY-SA 4.0)**

# O Evolucionismo e as origens do Ser Humano

- Utilizando a Teoria Evolucionista, os cientistas ligaram a origem do ser humano a um grupo de mamı́feros chamados primatas, que surgiu na África há cerca de 70 milhões de anos.
- Um grupo de primatas deve ter originado os primeiros hominı́deos, ou seja, seres que já apresentavam caracterı́sticas semelhantes às do homem moderno.
- **A evolução não é linear e ainda continua!**

# Os primeiros hominídeos

## Australopithecus

Viviam no sul da África, já andavam eretos sobre os dois pés e tinham habilidade com as mãos, mas não fabricavam instrumentos. 3,6 milhões de anos atrás - África.

## Homo Habilis

Já tinha a habilidade de fabricar utensı́lios simples de pedra. Alimentava-se de vegetais e carne, e provavelmente, desenvolveu uma forma de linguagem. 2 milhões de anos atrás - Leste da África.

## Homo Erectus

Além de utilizar utensı́lios de pedra lascada, como machados, era bom caçador. Foi provavelmente a primeira espécie a deixar a África. Entre 1,8 milhões de anos e 100-200 mil anos - África.

## Homo Neanderthalensis

Também conhecido como Homem de Neandertal. Muito parecido com o ser humano moderno, era caçador, e suas diferentes subespécies habitaram a Europa e a Ásia Central entre 230 mil e 30 mil anos atrás.

## Homo Sapiens

Surgiu entre 190 mil e 150 mil anos atrás, nas savanas africanas, e se espalhou por todos os continentes. Construiu instrumentos variados e mais sofisticados, desenvolveu a linguagem e expressões artı́sticas.

## Homo Sapiens Sapiens

O Homo Sapiens, cuja população se dividiria em sub-espécies que receberiam as denominações de Homo Sapiens Sapiens, Homo Sapiens Neanderthalensis e Homo Sapiens Denisovan. E uma prova de que todos pertenceriam à mesma espécie seria o fato de poderem se reproduzir entre si. Nós pertencemos à subespécie Homo Sapiens Sapiens!

# Da África para o mundo

Todas as evidências indicam que os seres humanos surgiram na África e, a partir dela, se espalharam para outros continentes. As rotas dessas migrações são baseadas em fontes e formulações de teorias.

# As Origens do Homem Americano

## Teoria de Clóvis

- Também conhecida como Teoria do Estreito de Bering, esta teoria foi proposta inicialmente no ano de 1590 d.C. por José de Acosta e passou a ser aceita em 1930.
- Tornou-se aceita cientificamente entre os anos de 1928 e 1937, quando foram encontrados, em escavações arqueológicas nas proximidades da cidade de Clovis (Novo México), nos Estados Unidos, artefatos de mesmo tipo dos anteriormente descobertos na região da Berı́ngia. Por isso, também é conhecida por “Teoria de Clovis”.

### O Estreito de Bering

- Durante a última era glacial, a concentração de gelo nos continentes fez descer o nível dos oceanos em pelo menos 120 metros. Esta descida provocou, em vários pontos do planeta, o aparecimento de diversas conexões terrestres.
- Um destes lugares foi a Beríngia. Devido a sua baixa profundidade (entre 30 e 50 metros), a descida do nível do mar colocou, a descoberto, um amplo território que alcançou 1.500quilômetros, unindo as terras da Sibéria e do Alasca, há aproximadamente 40.000 anos.

## Teoria Malaio-Polinesa

Esta teoria defende que diversas tribos teriam se utilizado de canoas primitivas e que, indo de ilha em ilha rumo a leste, teriam chegado na América do Sul. O principal defensor desta teoria foi o antropólogo francês Paul Rivet, que defendeu esta teoria em 1943. Não negava a passagem do homem pela Beríngia; apenas defendia que a chegada do homem na América teria ocorrido por mais de uma rota. Esta passagem teria ocorrido em dois momentos e em dois lugares diferentes. Primeiramente na Austrália, 6 000 anos antes da Beríngia; e na Melanésia um pouco mais tarde.

## Teoria de Walter Neves

- Essa hipótese vem trabalhando com a possibilidade de um outro movimento migratório acontecido a partir da navegação do Pacífico, através das ilhas polinésias até as regiões oeste e central das Américas. Nessa possibilidade, pressupõe a existência de uma rudimentar tecnologia de transportes que permitiu a realização gradual de pequenas navegações que favoreceram tal empreitada. Sendo assim, a ocupação do continente poderia ter acontecido em diferentes frentes.
- Walter Neves, um antropólogo brasileiro, desenvolveu ao longo de vinte anos a teoria que defende que o continente americano foi colonizado por duas ondas de Homo sapiens vindos da Ásia. A primeira onda de migração se acredita ter chegado há cerca de 14 mil anos e tinha sido composta por indivíduos com morfologia semelhante à dos Aborígenes e aos africanos, de morfologia negroide. A segunda onda migratória se acredita ter chegado no continente há cerca de 12 mil anos, e os membros deste grupo tinham as características físicas dos asiáticos, de quem os modernos povos indígenas possivelmente derivam.
- Uma investigação mais aprofundada e a medição de centenas de crânios da Serra da Capivara, incluindo o mais antigo, de uma jovem mulher que foi chamada de Luzia, levou Neves e outros arqueólogos a especularem uma incrível viagem por mar, da Austrália para o Brasil, que não teria sido realizada com conhecimento de rotas, mas por acidente.