# Plano de Aula de História
# Tempo Histórico e Tempo Cronológico
## 6º ano

**Prof. Mulin**

![](../../ccbysa4.jpg)

**Atribuição-CompartilhaIgual 4.0 Internacional (CC BY-SA 4.0)**

## Patrimônios

A UNESCO (sigla em inglês para Organização das Nações Unidas para a Educação, a Ciência e a Cultura) é uma organização internacional que atual para defender a herança natural, histórica e cultural dos povos. No Brasil a proteção é feita pelo IPHAN (Instituto do Patrimônio Histórico e Artı́stico Nacional).

## Tipos de Patrimônio

O Patrimônio Cultural é dividido em dois grupos, que variam de acordo com a sua natureza. São eles: **Patrimônio Imaterial** e **Patrimônio Material**. Além desses, há também o **Patrimônio Artı́stico**, que reúne os bens artı́sticos, e o **Patrimônio Natural**, referente aos bens naturais de uma região.

### Patrimônio Material

- Eu posso tocar nesse patrimônio!
- Diz respeito aos bens materiais, ou seja, tangíveis, de um povo. Abrange os museus, monumentos arquitetônicos, igrejas, bibliotecas, etc.
- Exemplos: Centro Histórico de Ouro Preto (Ouro Preto/MG), Conjunto Arquitetônico de Paraty (Paraty/RJ) e Centro Histórico de Olinda (Olinda/PE).

### Patrimônio Imaterial

- Eu NÃO posso tocar nesse patrimônio!
- É o tipo de patrimônio considerado intangível e abrange as expressões simbólicas e culturais de um povo, como as festas, as danças, músicas, saberes, costumes, formas de expressão, entre outros.
- Exemplos: funk carioca, frevo (PE), literatura de cordel e carnaval.

## Miracema - RJ

O município atualmente tem nada mais, nada menos, que 105 patrimônios tombados. Você sabia disso?

Várias ruas, casas, praças, capelas, entre outras construções e tradições locais são um verdadeiro museu a céu aberto.

São alguns deles: Praça Dona Ermelinda, Praça Ary Parreiras, Banda Sete de Setembro, Caxambu de Miracema, Companhia Folclórica Boi de Miracema, Hotel Braga, Fazenda Santa Inês, entre outros.

> A ideia dessa aula é apresentar aos alunos alguns patrimônios históricos de Miracema para que eles possam conhecer a História da própria cidade.