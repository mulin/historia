# Plano de Aula de História
# Tempo Histórico e Tempo Cronológico
## 6º ano

**Prof. Mulin**

![](../../ccbysa4.jpg)

**Atribuição-CompartilhaIgual 4.0 Internacional (CC BY-SA 4.0)**

## Reflexão

> Ao mostrar fotos surrelistas, questionar sobre o que é o tempo na visão de cada um.

> Que outros objetos podem substituir os relógios das fotos sem que elas percam o sentido?

## O que é tempo?

Algumas repostas possíveis:
- Período sem interrupções no qual os acontecimentos ocorrem: quanto tempo ainda vai demorar esta consulta? 
- Continuidade que corresponde à duração das coisas (presente, passado e futuro): isso não é do meu tempo. 
- O que se consegue medir através dos dias, dos meses ou dos anos; duração: esse livro não se estraga com o tempo. 
- Certo intervalo definido a partir do que nele acontece; época: o tempo dos mitos gregos. 
- Parte da vida que se difere das demais: o tempo da velhice.

> Quais as diferentes formas de se contar o tempo? Como nós, ocidentais, contamos o tempo e por que temos essa necessidade?

## Tempo Cronológico

> Questionar sobre como é a rotina nos dias de semana e nos finais de semana. A partir das resposta, estabelecer um consenso de que cada um tem uma cultura e uma vida diferentes. Por exemplo: alunos com religiões diferentes ou sem religião tem rotinas muito diferentes nos finais de semana.

O **Tempo Cronológico** é o tempo que pode ser contado em instrumentos como relógios e calendários. Esse tempo é usado para organição, que se pode marcar e contar. Ex: segundos, minutos, horas, dias, semanas, meses e anos. Ele tem **curta duração**.

## Tempo Histórico

> Fazer perguntas que exigem conhecimento de História por parte dos alunos. Muitos não conseguirão responder as perguntas, pois o tempo histórico, diferente do tempo cronológico, é um tempo que exige estudo de História para se conhecer.

O **Tempo Histórico** é o tempo em que acontecem os processos históricos. A cada novo processo histórico, se inicia um novo tempo histórico. Por esse motivo, a história foi dividida de acordo com cada novo processo. São eles: Pré-história, Idade Antiga, Idade Média, Idade Moderna e Idade Contemporânea. Ele tem **longa duração**.

## Divisão das Percepções de Tempo

O historiador Fernand Braudel dividiu as percepções de tempo em História em:

### Curta Duração

Em História, o tempo de curta duração é aquele que diz respeito aos eventos que fazem parte diretamente do cotidiano dos sujeitos, mas que tem significado social. Um crime, uma greve ou a inauguração de uma linha de metrô, por exemplo.

### Média Duração

Já o tempo de média duração é aquele que contém eventos que não são mais do cotidiano atual dos sujeitos, mas que podem ser conhecidos através de experiências passadas de pessoas que viveram aquela época. A migração dos europeus para o Brasil e a Segunda Guerra Mundial, por exemplo.

### Longa Duração

O tempo de longa duração é aquele que já entrou totalmente para os livros de História, onde os participantes já não estão mais vivos e só podem ser conhecidos pelo estudo da História, coisas como a Primeira Revolução Industrial ou a Descoberta das Américas.

## Formas de se contar o tempo

> Questionar sobre as formas de se contar o tempo e ultrapassar o senso comum onde muitos responderão "relógio", "calendário, "celular", etc. Trazer para o diálogo a forma de contagem do tempo por comunidades indigenas pré-colombianas, tribais africanas e culturas que contam o tempo de maneira diferente das sociedades cristãs, como judeus, muçulmanos e chineses.
