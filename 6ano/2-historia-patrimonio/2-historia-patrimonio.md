# Plano de Aula de História
# Tempo Histórico e Tempo Cronológico
## 6º ano

**Prof. Mulin**

![](../../ccbysa4.jpg)

**Atribuição-CompartilhaIgual 4.0 Internacional (CC BY-SA 4.0)**

## Para que serve a História

Toda sociedade passa por transformações, e muitas delas costumam ser muito demoradas a ponto de não serem percebidas e analisadas pelos indivı́duos que as vivenciaram.

Para estudar esses processos, desenvolveu-se um campo especializado do conhecimento: a história.

## O Trabalho do Historiador

O profissional que possui formação no curso de história acaba se tornando um historiador. Ele busca identificar o que mudou e o que permaneceu ao longo tempo, entender o passado e esclarecer a relação entre ele e o tempo presente.

Portanto, esse profissional estuda justamente tudo aquilo que já passou. A partir dos acontecimentos, pode analisar os fatos e também as informações sob diferentes ângulos, o que também acaba sendo fundamental para a humanidade como um todo.

O historiador escolhe, de acordo com a finalidade de sua pesquisa, os aspectos que irá estudar, as fontes que irá analisar, as opiniões que pretende discutir, os sentimentos que julga mais importantes.

Como se fosse um detetive, o historiador analisa um acontecimento com base em fontes históricas, aceita ou recusa interpretações já existentes, colhe depoimentos e chega a uma conclusão.

## Fontes Históricas

Os vestı́gios do passado usados pelo historiador em sua pesquisa são o que chamamos de fontes históricas. Em outras palavras, as fontes históricas são documentos que, através de seus sinais e interpretação, permitem que o historiador possa reconstruir e recontar a história.

## Tipos de Fontes

- **Fontes escritas**: documentos, atas, textos, livros e etc.
- **Fontes orais**: discursos, tradições, entrevistas, entre outros.
- **Fontes imagéticas**: fotografias, desenhos ou outro tipo de material artı́stico ou não.
- Existem também **fontes arquitetônicas**, **arqueológicas** e etc. Todas são úteis para a construção das narrativas históricas por parte do historiador.

## Patrimônios

A UNESCO (sigla em inglês para Organização das Nações Unidas para a Educação, a Ciência e a Cultura) é uma organização internacional que atual para defender a herança natural, histórica e cultural dos povos. No Brasil a proteção é feita pelo IPHAN (Instituto do Patrimônio Histórico e Artı́stico Nacional).

## Tipos de Patrimônio

O Patrimônio Cultural é dividido em dois grupos, que variam de acordo com a sua natureza. São eles: **Patrimônio Imaterial** e **Patrimônio Material**. Além desses, há também o **Patrimônio Artı́stico**, que reúne os bens artı́sticos, e o **Patrimônio Natural**, referente aos bens naturais de uma região.

### Patrimônio Material

- Eu posso tocar nesse patrimônio!
- Diz respeito aos bens materiais, ou seja, tangíveis, de um povo. Abrange os museus, monumentos arquitetônicos, igrejas, bibliotecas, etc.
- Exemplos: Centro Histórico de Ouro Preto (Ouro Preto/MG), Conjunto Arquitetônico de Paraty (Paraty/RJ) e Centro Histórico de Olinda (Olinda/PE).

### Patrimônio Imaterial

- Eu NÃO posso tocar nesse patrimônio!
- É o tipo de patrimônio considerado intangível e abrange as expressões simbólicas e culturais de um povo, como as festas, as danças, músicas, saberes, costumes, formas de expressão, entre outros.
- Exemplos: funk carioca, frevo (PE), literatura de cordel e carnaval.

## Atividades

1) Traga na próxima aula uma foto de sua famı́lia que você considera importante para sua memória.

2) Pesquise um patrimônio material ou imaterial de Miracema. Traga na próxima aula informações sobre esse patrimônio.