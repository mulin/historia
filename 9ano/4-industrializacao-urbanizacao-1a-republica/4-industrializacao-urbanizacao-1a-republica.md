# Plano de Aula de História
# Industrialização e Urbanização na Primeira República

**Prof. Mulin**

![](../ccbysa4.jpg)

**Atribuição-CompartilhaIgual 4.0 Internacional (CC BY-SA 4.0)**

## Introdução

A produção de café no Brasil no século XIX gerou um grande lucro que permitiu com que os cafeicultores investissem capital em outras atividades. Dessa forma, foi desenvolvida uma infraestrutura voltada para o escoamento do café que incluía:

- Ferrovias;
- Portos;
- Companhias de Navegação;
- Estradas;
- Produção de Energia Elétrica.

> Este processo está intimamente ligado à Revolução Industrial, ocorrida na Europa no final do século XVIII. Nessa época, os trabalhos manufaturados foram substituídos por máquinas a vapor, reinventando as dinâmicas de produção e as relações entre os trabalhadores. O capitalismo ganhava força e se tornava o modelo econômico vigente no mundo. A produção têxtil era a principal atividade desenvolvida no começo da era industrial, seguida pela construção das locomotivas e estradas de ferro.

> A Revolução Industrial no Brasil aconteceu de forma atrasada e pouco desenvolvida. Os principais impulsionadores deste movimento foram os cafeicultores do Estado de São Paulo que, visto o aumento da produção e exportação de café, decidiram investir em maquinário e, consequentemente, no crescimento do setor industrial.

A necessidade de crédito para investimento nos cafezais e nessas outras atividades estimulou a expansão dos bancos e das casas de exportação nas regiões produtoras. Dessa forma, o capital gerado pela exonomia do café possibilitou o investimento na **atividade industrial**. As cidades tiveram um rápido crescimento e as demandas por **bens não duráveis** aumentou, formando a base da industria nascente como alimentos, tecidos, sabões, bebidas, calçados e chapéus. Esse fenômeno também criou demada por mão-de-obra especializada.

## Os Imigrantes

Com a libertação dos escravizados através da Lei Áurea em 1888, houve uma forte demanda por mão-de-obra nas lavouras, e com o crescimento das cidades, mão-deobra especializada também era necessária para indústria, comércio, construção civil e outros serviços. Com isso, muitas pessoas se deslocaram do campo para os centros urbanos, causando um fenômeno conhecido como **Êxodo Rural**.

> Êxodo rural é o processo de migração de pessoas do campo para a cidade. Muitas causas podem ser associadas a ele, como a modernização da produção agrícola, a concentração fundiária, a busca por melhores condições de vida e melhores empregos, entre outros fatores. Entre as suas consequências estão o esvaziamento das zonas rurais e o crescimento desordenado das cidades.

Como o Brasil era um país escravista pouquíssimos anos antes da instauração da República, faltava mão-de-obra especializada para as cidades e para as lavouras, pois os escravizados não eram tratados como trabalhadores. A péssima condição de vida dos ex-escravizados e a falta de políticas públicas para a "correta" inserção do negro na sociedade fez com que ficassem marginalizados. Essa demanda por mão-de-obra especializada foi suprida por imigrantes estrangeiros que traziam conhecimentos e técnicas de seus países de origem.

> Entre 1887 e 1930, cerca de 3,8 milhões de estrangeiros se estabeleceram no Brasil. Eram principalmente italianos, espanhóis, alemães, portugueses, sírios, libaneses e japoneses.

A maior parte desses imigrantes foi para as regiões cafeeiras dos atuais estados de São Paulo, Rio de Janeiro e Minas Gerais. Descontentes com as condições de pobreza e exploração, muitos deles também deixaram o campo e foram para as cidades.

> Muitos dos imigrantes vieram para abrir seus negócios no Brasil ou para executar tarefas muito técnicas. No estado de São Paulo, em 1920, quase 65% das empresas pertenciam a estrangeiros.

A forte migração de brasileiros e imigrantes do campo para as cidades provocou o fenômeno da **urbanização**.

> Não se deve confundir crescimento das cidades com urbanização. O processo da urbanização ocorre quando a população do campo diminui e a da cidade aumenta em paralelo. Apenas o crescimento das cidades não se manifesta como um fenômeno de urbanização.

## As Reformas Urbanas

A urbanização no Brasil não aconteceu acompanhada de planejamento público. As cidades eram ocupadas de maneira irregular, com moradias inadequadas e falta de saneamento básico que facilitava a proliferação de epidemias.

Na época, esse crescimento desordenado notado na então capital da República, o Rio de Janeiro, fez com que o presidente da República Rodrigues Alves propusesse uma renovação urbana na cidade em 1902. Esse acontecimento ficou conhecido como **Reforma de Pereira Passos**, nome do engenheiro que tomou posse como prefeito no Distrito Federal (Rio de Janeiro).

Prédios foram demolidos para abertura de avenidas e prolongamento das ruas, calçamentos reformados e jardins e praças connstruídos. Essa grande reforma expulsou a população pobre do centro da cidade para regiões periféricas, fazendo com que surgissem assentamentos subnormais como cortiços e favelas na periferia da cidade. Dessa forma, a cidade se subdiviu em bairros nobres e regiões pobres.

> Muitos desses moradores pobres do centro da cidade eram negros ex-escravizados que buscavam melhores condições de vida na "Cidade Maravilhosa". Todavia, o preconceito racial, a falta de educação básica, falta de políticas públicas e escassez de oportunidades fizeram com que muitos desses moradores fossem viver em regiões muito pobres, além de praticarem crimes para sobreviver. Um reflexo disso é que a maior parte da população pobre e favelizada no Brasil atualmente é composta por pessoas negras.

> A Belle Époque brasileira, também conhecida como Belle Époque Tropical e Era Dourada, é a vertente sulamericana do movimento francês Belle Époque, baseado no Impressionismo e no Art Nouveau. Foi um período de cultura cosmopolita, de mudanças nas artes, na cultura, na tecnologia e na política do Brasil, entre 1870 e fevereiro de 1922 — fins do Império até a Semana de Arte Moderna. Esse movimento também influenciou a arquitetura das casas e prédios no Rio de Janeiro.

Em São Paulo as reformas dividiram a cidade em bairros nobres e bairros operários, que ficavam na várzea dos rios, próximos às fábricas.

Os modelos de reurbanização do Rio de Janeiro e São Paulo foi, no geral, seguido pelas demais capitais brasileiras da época.

### A Semana de Arte Moderna de 1922

A Semana de Arte Moderna de 1922 fez com que o movimento modernista atingisse seu ápice. Realizada no Teatro Municipal de São Paulo, reuniu intelectuais e artistas que tinham a intenção de "proclamar a segunda independência do Brasil", dessa vez cultural e moderna. Eles propunham a ruptura das manifestações artísticas brasileiras com o tradicionalismo e com a visão colonialista, por meio da apropriação de diversas influências culturais e da valorização da cultura popular.

## Referências

FERNANDES, Ana Claudia (Org.). Araribá Mais: História. 1.ed. São Paulo: Moderna, 2018.

FERNANDES, Florestan. A inserção do negro na sociedade de classes. Dominus Editora. São Paulo, 2 vols., 1965.

GOMES, Marcelo Abreu. Antes do Kasato Maru... Centenário da colônia agrícola japonesa da Fazenda Santo Antônio - Macaé e Conceição de Macabu-RJ. 1.ed. Conceição de Macabu: Macuco, 2008.

PETTA, Nicolina Luiza de. A Fábrica e a Cidade até 1930. São Paulo: Atual, 2009.
