# Plano de Aula de História
# Lutas pela Cidadania na Primeira República

**Prof. Mulin**

![](../ccbysa4.jpg)

**Atribuição-CompartilhaIgual 4.0 Internacional (CC BY-SA 4.0)**


## A Luta pela Cidadania

Como sabemos, o voto no Brasil era censitário e a maior parte da população não tinha direitos políticos por ser analfabera e pobre. As manifestações em prol desses direitos ocorriam sempre frente à muita violência do Estado e dos grandes proprietários de terras a fim de rechaçar os movimentos.

As Revoltas da Vacina e da Chibata ocorreram entre 1904 e 1910 na cidade do Rio de Janeiro, capital do país, e podem ser consideradas símbolos de protestos populares contra a opressão exercida pelos controladores do Estado.

### Revolta da Vacina

Juntamente à reurbanização da capital, foram promovidas campanhas de higienização da cidade e erradicação de doenças como febre amarela, varíola e peste bulbônica. E em 1904, foi aprovada uma lei que tornava obrigatória a vacinação contra a varíola. Isso se tornou um problema, pois não houve nenhum tipo de política pública conscientizasse a população acerca da importância da vacinação, então explodiu uma rebelião popular conhecida como Revolta da Vacina.

A população enfrentou nas ruas, durante vários dias, a polícia, tropas do exército e da marinha. Em 16 de novembro de 1904 a lei foi revogada, fazendo com que as manifestações se disperssassem. A revolta deixou trinta mortos, quase mil presos e 461 deportados para o Acre.

### Revolta da Chibata

Em 23 de novembro de 1910, marinheiros dos encouraçados Minas Gerais e São Paulo tomaram posse das embarcações a fim de exigir o fim dos castigos físicos que sofriam. Os cargos oficiais da marinha eram ocupados por membros das camadas mais ricas da sociedade, enquanto os marinheiros vinham de famílias pobres, e muitos deles eram ex-escravizados ou descendentes de africanos escravizados no Brasil.

Liderada por João Cândido, conhecido como Almirante Negro, a revolta se espalhou por outros navios de guerras. O presidente Hermes da Fonseca cedeu às exigências dos marinheiros e prometeu anistiar os revoltosos, mas após a rendição, prendeu e expulsou vários marinheiros da corporação.

> Importante salientar que, por serem negros, muitos oficiais entendiam que a disciplina entre os marinheiros só podia ser mantida com a mesma violência praticada antes nas grandes fazendas.

> Os marinheiros lutavam pelo fim dos castigos físicos como a chibata e o bolo (castigo aplicado que consistia em bater na mão com uma palmatória), pela educação dos oficiais e pelo aumento do soldo, que era a forma de remuneração da classe.

## A Organização dos Movimentos Negros no Pós-abolição

O Estado brasileiro não se responsabilizou pelo destino dos ex-escravizados após a abolição da escravidão no Brasil. Os liberto não foram inseridos na sociedade brasileira como cidadãos. As elites trataram de manter o modelo social desigual e excludente a fim de manter seus privilégios. As ideias dos grupos abolicionistas que visavam reforma agrária, reforma eleitoral, acesso à educação, atendimento médico, emprego e salários dignos foram sufocadas pelas classes vigentes.

Para fazer frente às práticas racistas, foram criadas associações que reunião pessoas negras para se pensar essas questões e acabaram por contribuir com a difusão de pensadores negros. Houve ainda a fundação de diversos jornais independentes que compunham a **imprensa negra**, que colaborou com o desenvolvimento das lutas por democracia, igualdade, participação política da população afro-brasileira, além de contribuir no processo de consolidação dos movimentos negros contemporâneos brasileiros.

Será a arte, porém, que contará com a maior adesão dos negros como forma de preservar sua identidade, ao mesmo tempo que absorvem outras influências. É o caso do surgimento do choro, o primeiro gênero musical brasileiro e dos ranchos e agremiações em torno do samba.

## A Organização dos Trabalhadores

O crescimento das cidades também fez com que a classe operária crescesse no país. As condições de trabalho da classe eram péssimas.

- Jornada de trabalho entre 14 e 16 horas diárias;
- Não havia cobertura médica;
- Não havia indenização por acidentes;
- Não havia direito à férias remuneradas;
- Não havia direito a salário mínimo;
- As mulheres e crianças recebiam salários mais baixos que dos homens;
- Não havia direito à licença maternidade;
- O trabalho infantil não era proibido.

As mulheres e crianças trabalhavam principalmente no setor têxtil. As mulheres chegaram a ocupar 58% dos cargos do setor.

A maior parte dos operários era imigrantes e foi deles que vieram as **ideias socialistas, comunistas e anarquistas que se difundiram no meio fabril**. O anarcossidicalismo foi a principal corrente da época e visava reunir militantes nos sindicatos. Os métodos de lutas dos sindicatos eram, principalemente, boicote, sabotagem e organização de greves.

> Sindicato é uma associação estável e permanente de trabalhadores tanto urbano-industrial, como rurais e de serviços, que se unem a partir da constatação e resolução de problemas e necessidades comuns.

### Anarcossindicalismo na Primeira República

O anarquismo é considerado uma doutrina sócio-política que surgiu entre os séculos XVII e XVIII na Europa, tendo chegado ao Brasil em 1850 através dos imigrantes europeus. O anarquismo defende que a sociedade não deve ter nenhuma forma de autoridade e considera que o Estado é uma força coercitiva.

As teorias do anarco-sindicalismo chegaram ao Brasil através de livros dos sindicalistas franceses, e difundiram-se através da imprensa e das decisões da grande massa operária, dominada por anarco-sindicalistas.

Durante a República Velha, a ideologia anarquista defendeu a organização sindical autônoma, a extinção do Estado, da Igreja e da propriedade privada. Se posicionaram contra a organização dos partidos políticos, divulgando suas ideias por meio de jornais, revistas, livros, panfletos, etc.

A ação direta era pregada pelo chamado “sindicalismo revolucionário”. Esta ação direta consistia em greves, boicotes e sabotagens e era considerada um meio para os trabalhadores adquirirem melhores condições de trabalho, indo contra o sistema capitalista. Em busca de destruir este sistema, os trabalhadores eram ensinados a se prepararem para uma ação final: a greve geral que destruiria este sistema. Nestas “ações diretas”, a violência era considerada aceitável, a sabotagem era considerada eficaz, e caso não fosse possível realizar a greve, o proletariado era orientado a destruir os equipamentos, os ambientes de trabalho, e pressionar os seus patrões até que suas exigências fossem atendidas.

### A Greve Geral de 1917

Em junho de 1917, uma greve geral paralisou por três dias a cidade de São Paulo. Anarquistas, apoiados por socialistas, formaram o **Comitê de Defesa Proletária**.

A participação de mulheres anarquistas foi de suma importância nesse movimento. Principalmente as que pertenciam ao **Centro Feminino de Jovens Idealistas**, em especial Rosa Musitano e Maria Angelina Soares, que era secretária da Liga Operária da Mooca.

Um acordo foi feito a partir das pressões dos grevistas. Foram algumas conquistas dos trabalhadores:

- Aumento de 20% sobre os salários;
- Direito de associação dos operários;
- Não demissãodos grevistas.

Contudo, o acordo não foi cumprido por grande parte dos industriais e nos meses seguintes líderes anarquistas foram presos e deportados.

## O Modernismo

O final do século XIX e início do século XX foram marcados por uma grande efervescência cultural na Europa, caracterizada pelos avanços tecnológicos que despertava debates e novas propostas artísticas. Alguns artistas brasileiros entraram em contato com essas discussões e fizeram nascer um movimento conhecido como Modernismo.

A nova elite brasileira que surgiu com a riqueza gerada pelos cafezais e pelas indústrias queriam dominar também os campos das ideias e da cultura, discordando das antigas elites que valorizavam apenas a cultura europeia tradicional, essa nova elite passou a incentivar os artistas modernistas brasileiros.

> Os artistas e intelectuais modernistas queriam criar uma nova cultura nacional conectada com as grandes transformações da industria. Para isso, eles pretendiam unir as vanguardas artísticas europeias, especialmente o Cubismo e o Expressionismo, com manifestações da cultura popular de origem africana e indígena, até então desprezadas pela antiga elite brasileira.

## Referências

FERNANDES, Ana Claudia (Org.). Araribá Mais: História. 1.ed. São Paulo: Moderna, 2018.

FERNANDES, Florestan. A inserção do negro na sociedade de classes. Dominus Editora. São Paulo, 2 vols., 1965.

NASCIMENTO, Álvaro Pereira do. "Sou Escravo de Oficiais da Marinha: a grande revolta da marujada negra por direitos no período pós-abolição (Rio de Janeiro, 1880-1910). Revista Brasileira de História, São Paulo, v.36, n.72, agosto de 2016. p.159-161.

PETTA, Nicolina Luiza de. A Fábrica e a Cidade até 1930. São Paulo: Atual, 2009.

SAMIS, Alexandre. Sindicalismo e Anarquismo no Brasil (1903-1934). Instituto de Teoria e História Anarquista, 2013. Disponível em: https://ithanarquista.wordpress.com/2013/01/14/alexandre-samis-sindicalismo-e-anarquismo-no-brasil-1903-1934/. Acesso em 28 de março de 2022.