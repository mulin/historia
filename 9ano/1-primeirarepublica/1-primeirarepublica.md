# Plano de Aula de História
# Proclamação da República no Brasil

**Prof. Mulin**

![](../ccbysa4.jpg)

**Atribuição-CompartilhaIgual 4.0 Internacional (CC BY-SA 4.0)**

## Introdução

Em 1889, um ano após a assinatura da Lei Áurea (explicar a forte influência dessa lei na mudança de posicionamento político das oligarquias rurais), a monarquia foi substituída pela República. Todavia, o sentido de evolução está atrelado à ótica do leitor, visto que não foi capaz de eliminar os valores da prática escravista.

O voto nesse período era censitário, ou seja, não havia eleições diretas no Brasil como atualmente. No caso do Brasil, apenas homens brancos, maiores de 21 anos, alfabetizados e com renda e terras suficientes. Mesmo com essas limitações, essa mudança eleitoral foi muito significativa.

> REFLEXÃO: **Qual a importância do voto? Por que é importante a fiscalização dos eleitos, independente de voto pessoal?**

> Importância da promoção da justiça e da igualdade social.

## Crise do Segundo Reinado

A partir de 1870, diversos grupos passaram a divergir do império e questionar a autoridade do imperador D. Pedro II, o que resultou em algumas questões:

### Questão Militar

Após o fim da Guerra do Paraguai, muitos ideais positivistas e republicanos ganharam relevância nos setores militares. Há de se observar também, que na falta de soldados, muitos escravizados foram enviados para a guerra mesmo sem direito ao serviço militar. Alguns autores defendem a hipótese de que a convivência de soldados brancos com negros nas trincheiras aproximou seus ideais do escopo abolicionista.

Os militares nesse período não tinham direito à participação política, e isso agravou as tensões entre o Império e os grupos militares. Eles representavam o setor positivista que fazia frente ao reinado de D. Pedro durante a crise do Segundo Reinado.

> Explicar a origem do positivismo na sociologia comteana. Sua máxima era "o amor por princípio, a ordem por base e o progresso por fim", que influenciou a frase central da bandeira nacional.

> Questionar também sobre as cores da bandeira, que não representam as matas, o ouro ou o céu, mas as cores das famílias Orleans e Bragança, que não foram alteradas na bandeira republicana. Mais um traço simbólico de que muito da cultura colonial e imperial prevaleceu no decorrer da república.

### Questão Religiosa

O imperador interferia em diversas decisões da Igreja Católica no Brasil a partir de duas leis: o padroado, que permitia que o imperador indicasse cargos eclesiásticos a fim de manter sua influência dentro do alto clero; e o beneplácito, que dava poderes ao imperador de anular ou não a vigência de bulas papais em território nacional. A insatisfação do clero católico brasileiro o levou a apoiar a proclamação da república.

### Questão Abolicionista

Apesar das pressões britânicas para que o Brasil abolisse a escravidão, o Império Brasileiro ofereceu forte resistência, mesmo após a proibição do tráfico negreiro no Atlântico pela Inglaterra. A questão era política. A mão-de-obra escrava permitia os altos lucros das oligarquias rurais, que, por conseguinte, eram a principal base de apoio político do imperador D. Pedro II.

A alforria dos escravizados ocorreu de forma gradativa com a criação de leis como Euzébio de Queiroz (1850), Ventre Livre (1871), Sexagenários (1885) e, finalmente, pela Lei Áurea (1888).A abolição aconteceu sem nenhum pagamento de indenização. Com isso, os fazendeiros romperam com Dom Pedro II e se aproximaram do movimento republicano. 

> Refletir sobre o papel problemático dessas leis, visto que o tráfico não foi definitivamente cessado por conta da pirataria. As crianças nascidas após 1871 continuavam nas fazendas. Os escravizados dificilmente chegavam aos 65 anos de idade, por conta de sua péssima qualidade de vida. E, por fim, a Lei Áurea não foi acompanhada por políticas públicas a fim de inserir os negros na sociedade. Nesse sentido, retomar a reflexão de liberdade e justiça social com os alunos.

> Refletir sobre o uso inadequado da palavra "escravo", substituindo-a pelo termo "escravizado", pois o termo vai denotar uma condição imposta e não uma característica natural e inerente ao indivíduo escravizado.

### E o povo?

Poucos setores da classe média urbana aderiram aos movimentos republicanos, entre eles estavam os intelectuais e homens ricos. Há de se observar aqui que as políticas do Segundo Reinado eram voltadas para a economia rural, principalmente do café. Até mesmo a industrialização nas cidades era voltada principalmente para usinas de açúcar e/ou processamento do café.

A população mais pobre, sem acesso a informação, não alfabetizada, assistiu a tudo "bestializada". Há relatos de que a notícia de que o Brasil era uma república chegou a algumas cidades apenas quatro anos após a proclamação da mesma.

> Sobre a demora na chegada das notícias, estabelecer um comparativo entre a velocidade em que as notícias correm atualmente pela internet e como era naquele período, onde era necessário dias ou semanas de viagem em tropas de muares a fim de levar a informação através de jornais impressos (que também era censurados pelo Império).

## A Proclamação da República

A Proclamação da República no Brasil foi um golpe de Estado promovido pelos militares positivistas com apoio das oligarquias rurais e da classe média urbana. No dia 15 de novembro de 1889, sob o comande Marechal Deodoro da Fonseca, a república foi proclamada no Brasil, dando início ao período conhecido como Primeira República (1889-1930)

> Questionar com os alunos o conceito de "Herói". Refletir sobre a cultura entre os militares de sempre promoverem "heróis" e seus atos, colocando monumentos e pinturas de militares em espaços públicos.

> Questionar o status "heroico" de figuras como Deodoro da Fonseca e Duque de Caxias (falar sobre as atrocidades durante a Guerra do Paraguai).

Dessa forma, formou-se um governo provisório comandado pelo marechal Deodoro da Fonseca e a família real foi expulsa do Brasil, exilando-se, assim, em Portugal. Deodoro tomou algumas medidas em seu curto período no executivo:

- Dissolução das Assembleias Provinciais;
- Dissolução das Câmaras Municipais;
- Destituição dos presidentes dos estados;
- Convocação de eleições para o Congresso Nacional.

> Salientar a diferença de nomenclaturas no poder executivo dos estados brasileiros, que até os anos 1940 eram chamados "presidentes dos estados", que atualmente são os governadores.

O Congresso promulgou uma nova constituição em 24 de fevereiro de 1891 e, no dia seguinte, elegeu Deodoro como presidente e Floriano Peixoto como seu vice.

## Constituição de 1891

A primeira constituição republicana tinha fortes tendências iluminista e, por consequência, liberais. Afirmava o direito à igualdade, à liberdade, à segurança e à propriedade.

> Retomar estudos acerca do Iluminismo e Revolução Francesa do 8º ano.

> Refletir sobre quem tinha acesso a esses direitos durante a Primeira República. Como era a situação dos ex-escravizados e os setores mais carentes da sociedade que estavam submissas a uma relação de poder (simbólico e objetivo) por parte das oligarquias rurais.

> Refletir sobre a afirmação de acesso à segurança e direito à propriedade e igualdade comparando-os com a situação dos ex-escravizados, que não foram inseridos na sociedade brasileira de forma igualitária após a assinatura da Lei Áurea. Vivendo marginalizados em cortiços (tratar do surgimento dos assentamentos subnormais e favelizados compostos em sua maioria por populações negras próximo ao Morro do Castelo no Rio de Janeiro. Muitos deles era ex-combatentes da Guerra de Canudos que apelidaram o morro de favela, por conta da planta faveleira, muito comum no nordeste brasileiro.

Algumas características da primeira Constituição:

- Tripartição de poderes: executivo, legislativo e judiciário;
- Dissolução do Poder Moderador;
- Direito ao voto. Voto censitário: soldados, religiosos, mendigos e mulheres não tinham esse direito;
- Separação entre Estado e Igreja.

> Explicar cada um dos três poderes.

> Tratar da forte influência da Igreja Católica nos municípios brasileiros até os dias atuais, apesar da separação entre os governos e a Igreja enquanto instituição.

> Analisar a presença de igrejas, capelas e locais católicos no município e a influência do padre em eventos realizados em locais públicos que, provavelmente, alguma outra religião não cristã não conseguiria aval da prefeitura para realização.

## Referências Bibliográficas

BRAICK, Patrícia Ramos; BARRETO, Anna. Estudar História: das origens do homem à era digital (manual do professor) 3 ed. São Paulo: Moderna, 2018.
FERNANDES, Florestan. A Integração do Negro na Sociedade de Classes. 3 ed. São Paulo: Ática, 1978, v.1.
