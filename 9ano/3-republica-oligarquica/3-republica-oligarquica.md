# Plano de Aula de História
# República Oligárquica (1894 - 1930)

**Prof. Mulin**

![](../ccbysa4.jpg)

**Atribuição-CompartilhaIgual 4.0 Internacional (CC BY-SA 4.0)**

## Introdução

Após a queda do governo provisório encabeçado por Deodoro da Fonseca e Floriano Peixoto, o primeiro presidente civil, Prudente de Morais, assume a presidência. As alianças entre as oligarquias estaduais passaram a determinar os rumos do país. Por isso esse período ficou conhecido como **República Oligárquica** ou **República das Oligarquias**.

## Política dos Governadores

A Política dos governadores era um sistema de alianças entre o governo federal e os governos estaduais, que eram controlados pelas oligarquias locais. O governo federal oferecia favores políticos e não interviam nas disputas locais, desde que os presidentes dos estados elegessem deputados e senadores que não fossem um crivo para os interesses do presidente. Para atingir esse objetivo, as eleições eram diretas e fraudulentas, através, principalmente, do **voto de cabresto**, as oligarquias locais conseguiam eleger seus candidatos.

Antes dos anos 1990 a historiografia tratava de um certo controle das oligarquias rurais do estado de São Paulo, que era o maior produtor de café, e das oligarquias do estado de Minas Gerais, que era o maior produtor de pecuária leiteira, na política nacional. Os historiadores caracterizaram esse fenômeno como **Política do Café com Leite**. Todavia, este paradigma foi quebrado ao concluir que as oligarquias de todos os estados sabiam de seu potencial de intervenção e agiam conforme seus interesses.

## Coronelismo e Currais eleitorais

As maiores fazendas dos municípios tinham o que era conhecido como "curral". Naquela época não se tratava de um local onde se colocavam os bois, mas um local de escambo de produtos agrícolas. Muitos moradores se dirigiam ao curral para trocar excedentes por outros produtos que eram necessários para sua sobrevivência.

O oligarga que controlava o curral local era chamado de **coronel** e tinha muita influência sobre a população local, atingindo seus objetivos eleitorais através da violência ou da troca de favores, oferecendo empregos, roupas, alimentos, medicamentos, etc. para os mais necessitados. Como o voto não era secreto, era mais fácil coagir o eleitorado através do voto de cabresto.

> **Coronel** é um termo que tem origem na Guarda Nacional do Império. As tropas eram compostas por homens livres e proprietários de terra, mas que durante a Primeira República, foram submetidas ao exército. Todavia, o termo coronel continuou a ser utilizado para designar homens poderosos dos municípios, proprietários de terra e influentes na política local.

> Refletir sobre a prática de venda e compra de votos nos dias atuais.

## Conflitos Rurais na Primeira República

Cerca de 70% da população brasileira habitava o campo no início da Primeira República. A maioria dos trabalhadores rurais estavam submetidos ao poder de grandes proprietários, o que colaborou para a eclosão de diversos conflitos. Os principais foram a Guerra de Canudos, o Cangaço e a Guerra do Contestado.

### Guerra de Canudos

Antônio Vicente Mendes Maciel, conhecido como **Antônio Conselheiro**, peregrinou pelo sertão nordestino por muitos anos pregando mensagens religiosas à população mais pobre. Ele atriu muitos seguidores que, em 1893, se fixaram em Canudos, no norte da Bahia, e fundaram o arraial de Belo Monte. O arraial tinha uma população com número incerto aos historiadores que vão de 10 mil a 30 mil pessoas. O arraial vivia da agricultura de subsistência e dividia a produção entre seus moradores. O que sobrava era vendido nas vilas vizinhas.

O arraial logo passou a ser considerado uma ameaça ao poder dos latifundiários, da Igreja e da república recém-instalada. Em 1896 o governo da Bahia enviou uma expedição armada ao local, mas os soldados foram desmobilizado pelos **conselheiristas**. Em 1897 o governo enviou mais duas expedições que também foram derrotadas. Apenas em agosto de 1987, com a quarta expedição enviada pelo governo federal, o arraial foi derrotado e completamente destruído.

### Cangaço

Os bandos armados eram conhecidos no sertão nordestino bem antes da Primeira República. Esses grupos eram chamados de cangaceiros e eram sustentados pelos chefes políticos locais. Todavia, esses bandos passaram a agir de maneira independente atacando fazendas, saqueando comércios e matando aligarcas locais.

Embora cometessem crimes, os cangaceiros eram vistos por muitos sertanejos como heróis por terem coragem de enfrentar a polícia e os coronéis que subjulgavam a população local. O cangaceiro mais famoso foi Virgulino Ferreira da Silva, conhecido como **Lampião**, que percorreu o sertão com seu bando entre 1910 e 1938, quando foram mortos em uma emboscada na Fazenda Angicos, onde se escondiam.

O Cangaço é objeto de diferentes interpretações. Alguns pesquisadores vêem o movimento como uma reação rebelde da população sertaneja contra a repressão de oligarcas e do governo. Outros vêem os cangaceiros como bandidos, que não tinham nenhum projeto de transformação social e praticavam crimes para sobreviver.

### Guerra do Contestado

A Guerra do Contestado ocorreu entre os anos de 1912 e 1916 na divisa dos estados do Paraná e Santa Catarina. Em 1908, o governo federal permitiu que empresas estrangeiras construíssem uma rodovia ligando São Paulo ao Rio Grande do Sul e extraíssem madeira da região. Com isso, pequenos proprietários que viviam na área há quase cem anos foram expulsos da divisa do estado.

Naquela região surgiu a figura de **José Maria de Jesus**, um líder religioso, que assim como Antônio Conselheiro no Nordeste, conquistou centenas de seguidores que buscavam melhores condições de vida e, inclusive, operários que trabalhavam na construção da ferrovia. Eles organizaram uma comunidade igualitária que foi chamada de Monarquia Celeste, no centro-sul de Santa Catarina.

Assim como Canudos, o crescimento da comunidade foi considerado uma ameaça para os governos estaduais de Santa Catarina e Paraná. Tropas armadas foram enviadas com ajuda do governo federal em 1912 e o conflito perdurou até 1916. As tropas assassinaram José Maria de Jesus em 1912 e os rebeldes foram derrotados em 1916.

> Os seguidores de José Maria de Jesus acreditava que após a sua morte ele teria se juntado ao exército encantado de D. Sebastião, rei português que desapareceu na Batalha de Alcácer-Quibir, em 1578. Muitos da colônia brasileira acreditava que D. Sebastião voltaria para fundar um reino de liberdade e justiça, como um messias. Canudos e COntestado são considerados **movimentos messiânicos**.

## Referências Bibliográficas

BRAICK, Patrícia Ramos; BARRETO, Anna. Estudar História: das origens do homem à era digital (manual do professor) 3 ed. São Paulo: Moderna, 2018.

HOBSBAWM, E.J. Bandidos. Rio de Janeiro: Forense Universitária, 1969.

MELLO, Frederico Pernambucano. Guerreiros do sol – violência e banditismo no nordeste do Brasil. São Paulo: A Girafa Editora, 2004.