# Plano de Aula de História
# República da Espada (1889-1894)

**Prof. Mulin**

![](../ccbysa4.jpg)

**Atribuição-CompartilhaIgual 4.0 Internacional (CC BY-SA 4.0)**

## Governo Provisório

Período inicial da república brasileira onde os dois primeiros presidentes foram militares. Nesse primeiro momento, Marechal Deodoro assumiu o cargo de GOVERNO PROVISÓRIO. O país ainda não tinha um presidente interino.

Ao assumir o governo, Deodoro da Fonseca convocou uma Assembleia Constitucional para substituir as leis imperiais. A nova constituição foi promulgada no dia 24 de fevereiro de 1891 e tinha **características liberais**. No dia seguinte, Marechal Deodoro da Fonseca foi eleito presidente do Brasil pelo congresso e seu vice eleito foi o Marechal Floriano Peixoto.

Dessa forma, formou-se um governo provisório comandado pelo marechal Deodoro da Fonseca e a família real foi expulsa do Brasil, exilando-se, assim, em Portugal. Deodoro tomou algumas medidas em seu curto período no executivo:

- Dissolução das Assembleias Provinciais;
- Dissolução das Câmaras Municipais;
- Destituição dos presidentes dos estados;
- Convocação de eleições para o Congresso Nacional.

> Salientar a diferença de nomenclaturas no poder executivo dos estados brasileiros, que até os anos 1940 eram chamados "presidentes dos estados", que atualmente são os governadores.

## Primeira Constituição Republicana

A primeira constituição republicana tinha fortes tendências iluminista e, por consequência, liberais. Afirmava o direito à igualdade, à liberdade, à segurança e à propriedade.

> Retomar estudos acerca do Iluminismo e Revolução Francesa do 8º ano.

> Refletir sobre quem tinha acesso a esses direitos durante a Primeira República. Como era a situação dos ex-escravizados e os setores mais carentes da sociedade que estavam submissas a uma relação de poder (simbólico e objetivo) por parte das oligarquias rurais.

> Refletir sobre a afirmação de acesso à segurança e direito à propriedade e igualdade comparando-os com a situação dos ex-escravizados, que não foram inseridos na sociedade brasileira de forma igualitária após a assinatura da Lei Áurea. Vivendo marginalizados em cortiços (tratar do surgimento dos assentamentos subnormais e favelizados compostos em sua maioria por populações negras próximo ao Morro do Castelo no Rio de Janeiro. Muitos deles era ex-combatentes da Guerra de Canudos que apelidaram o morro de favela, por conta da planta faveleira, muito comum no nordeste brasileiro.

Algumas características da primeira Constituição:

- Tripartição de poderes: executivo, legislativo e judiciário;

> Explicar cada um dos três poderes.

- **Executivo**: presidente, vice-presidente e ministros de Estado;
- **Legislativo**: deputados federais e senadores;
- **Judiciário**: juízes dos tribunais federais e do Supremo Tribunal Federal.

- Dissolução do Poder Moderador;
- Direito ao voto. Voto censitário: soldados, religiosos, mendigos e mulheres não tinham esse direito;

> O voto aberto prevaleceu na maior parte das eleições;
> QUEM NÃO PODIA VOTAR?
> Menores de 21 anos;
> Analfabetos;
> Soldados;
> Religiosos;
> Mendigos;
> Mulheres.

- Separação entre Estado e Igreja.

Separação entre Estado e Igreja e a criação de mecanismos de registro civil, como as certidões nascimento, casamento e óbito.

> Tratar da forte influência da Igreja Católica nos municípios brasileiros até os dias atuais, apesar da separação entre os governos e a Igreja enquanto instituição.

## Encilhamento

Em 1890, o ministro da agricultura Rui Barbosa aumentou a impressão de papel moeda a fim de expandir o crédito para a agricultura e para a indústria. Todavia, a facilidade do crédito também estimulou o surgimento de empresas fantasmas, o que estimulou enganosamente o mercado. Muitos empresários aplicaram dinheiro no mercado financeiro, gerando uma intensa especulação financeira, que ficou conhecida como **Encilhamento**.

> Explicar a origem da palavra encilhamento: derivada da palavra *cilha*, uma espécie de correia que prende a sela aos cavalos antes do início de uma corrida.

> Especulação financeira: operação comercial sobre o mercado financeiro a fim de obter mais lucro em menos tempo.

> A crise foi solucionada apenas entre 1898 e 1902 durante a vigência do governo Campos Sales.

A moeda perdeu seu valor, pois as empresas não cresciam na mesma proporção da ações, fazendo com que a inflação explodisse. Muitas empresas faliram e o desemprego aumentou, além de gastos altíssimos do governo para socorrer as empresas gerarem uma crise econômica.

A crise do Encilhamento alertou as oligarquias regionais, que exigiam uma participação mais ativa na definição dos rumos econômicos do Brasil e a redução da forte centralização do poder nas mãos do presidente.

Para assegurar seus poderes, Deodoro dissolveu o Congresso e defendeu reformas na constituição. O que agravou ainda mais o descontentamento dessas oligarquias.

Pressionado, Deodoro renunciou em novembro de 1891. Marechal Floriano Peixoto, seu vice, assumiu o cargo.

## Primeiras Medidas do governo Floriano Peixoto

Empregou esforços para controlar a inflação e recuperar a economia: controlou preço dos alimentos, aluguéis e isentou alguns impostos. O que garantiu algum apoio popular ao governo.

Enfrentou as duas principais rebeliões do período: a REVOLUÇÃO FEDERALISTA e a REVOLTA DA ARMADA. Nos dois casos a repressão foi violenta.

## Revolução Federalista

- Estourou em fevereiro de 1893 no Rio Grande do Sul e estendeu-se por Santa Catarina e Paraná.
- Defendiam um  Estado menos centralizado.
- A revolta foi sufocada pelo governo em 1895.

## Revolta da Armada

- Explodiu em setembro de 1893.
- Os oficiais da marinha estava descontentes com a preponderância do exército e com a limitada participação da armada na política brasileira.
- Eles se rebelaram e pediram novas eleições.
- Durante o conflito, a cidade do Rio de Janeiro chegou a ser bombardeada pelos rebeldes, que se renderam apenas em março de 1894.

## Atividades

1. Explique o que foi a Crise do Encilhamento.
2. Por que a República da Espada recebeu esse nome?
3. Quais foram os dois primeiros presidentes da república?
4. O que foi a Revolução Federalista?
5. O que foi a Revolta da Armada?